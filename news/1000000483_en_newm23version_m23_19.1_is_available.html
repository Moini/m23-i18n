m23 19.1 brings support for clients with Linux Mint 19 and 19.1
<figure class="articleimg left" >
	<img src="gfx/LM-19.1-3-Cinnamon-Mate-Xfce.png" alt="Desktop environments<br>
	for Linux Mint 19 and 19.1" />
	<figcaption>Desktop environments<br/>
	for Linux Mint 19.x</figcaption>
</figure>

<p>
    <i>With <b><a href="https://en.wikipedia.org/wiki/Linux_Mint">Linux Mint</a> 19 and 19.1</b>, two new items were added to the list of distributions that m23 can install. For each distro, there are three preconfigured <b>desktop environments</b> available. In addition to these obvious changes, a lot of time was spent working on the <b>m23-autoTest framework</b> for automated testing of the m23 software. Other work areas include the refactoring of the MDK (m23 development kit) and <b>lots</b> of minor improvements. The amount of updates made to the code would easily be enough for two or three new versions of m23.</i>
</p>

<h3 style="display: inline;">LinuxMint 19 and 19.1</h3>

<p>From this version on, m23 supports the <a href="https://en.wikipedia.org/wiki/Linux_Mint">Linux Mint</a> versions <b>19 Tara</b> and <b>19.1 Tessa</b> for installation on the clients, with the <b>full range of functionality</b> that users already know from other distributions included in m23. There are three preconfigured <b>desktop environments</b> included for Tara and Tessa: <a href="https://en.wikipedia.org/wiki/MATE_(software)">Mate</a>, <a href="https://en.wikipedia.org/wiki/Cinnamon_(software)">Cinnamon</a> and <a href="https://en.wikipedia.org/wiki/Xfce">Xfce</a>. m23 can install both the 32- and 64-bit variants, leaving you the free choice among the available desktop environments, regardless of the system architecture.</p>

<h3 style="display: inline;">m23 autoTest</h3>

<p>This version of m23 is probably by far the most thoroughly tested version in history. This was made possible by the new <b>m23 autoTest framework</b> for <b>running automated test</b>. In countless test cycles, the framework allowed to test the combinations of m23 servers running on different platforms (Debian 9 (32 and 64bit), UCS 4.3, UCS 4.4, m23 installation ISO) with m23 clients of all supported client distributions systematically for the first time. This way, even errors were found that only occur very sporadically.</p>

<p>The most time-consuming development work done for creating the new version was the creation of the <b>m23-autoTest framework</b> for fully automated testing, end-to-end, from the m23 interface to the installation of new m23 clients in virtual machines (VMs).
</p>

<p>m23 autoTest simulates a human administrator's behavior by making use of <b>remote control</b> (<a href="https://en.wikipedia.org/wiki/Selenium_(software)">Selenium</a>) for the m23 web interface. Forms in dialogs are filled in automatically to e.g. create new m23 clients, to partition and format their hard disk or to install distributions with a graphical user interface.</p>

<p>One <b>test cycle</b> includes the installation of <b>an m23 server</b> and of (currently) <b>18 m23 clients</b> that this new server will set up. For each distribution, two clients will be installed as 32 and 64 bit variants, with randomly assigned desktop environments and alternating between German, English and French language settings.</p>

<p>All tests are performed on virtual machines, to ensure fast execution and good reproducibility. At various <b>check points</b> (e.g. notifications in the VM screen that are identified via OCR, messages in the m23 interface, or return values of commands executed in the VM), the <b>installation status is continually monitored and evaluated</b>. This information enables m23 autoTest to react to events with e.g. simulated key strokes or actions in the web interface of m23. In case of critical errors (e.g. server services not running), m23 autoTest can abort the current test cycle. For the <b>ensuing analysis</b>, the VM's screen is recorded to <b>video</b>, and an <b>installation log file</b> is being written, which contains lots of additional information when created in debug mode.</p>

<p>Even if the range of functionality is clearly aimed at use for m23, autoTest could still be useful <b>for other projects</b>. More information (about installation and configuration of the autoTest environment, how to write own XML files that describe the tests, a <b>video recording</b> of an automated test installation of Linux Mint etc.) is available at <a href="https://goos-habermann.de/howto-10030-m23-autoTest">goos-habermann.de</a>.</p>

<h3>MDK (m23 Development Kit)</h3>

The MDK has been considerably improved in many sections, especially those that are used for the creation of boot media and their components:
<ul>
    <li><b>Compiling</b> the Linux kernel and BusyBox for <b>64 bit</b> now works again on a <b>32 bit m23 server</b> and vice versa.</li>

    <li>The built-in <b>documentation</b> has been updated and extended.</li>

    <li>During the creation of boot media for m23 clients or servers, all <b>required libraries</b> will now reliably be <b>found and included</b>.</li>

    <li>The software for the boot media will now be assembled from <b>Debian 8 and 9</b>. Package sources and packages that are available in older Debian versions only, have been removed.</li>

    <li><b>Additional notifications</b> make it easier to <b>spot errors</b>.</li>
</ul>


<h3>Bug fixes</h3>

<ul>
    <li>The current time is now synced between the m23 client and the m23 server before an SSL connection is initiated. The validity period of new SSL certificates now starts on 2019-01-01.
This will help avoid <b>issues</b> with <b>SSL certificates</b> that can, for example, occur in differing time zones.</li>
       
    <li>When transferring the minimal-sized distribution archive file from the m23 server to the m23 client during the tests, the m23 server in some rare cases delivered the corresponding signature file instead of the selected archive. A new routine now <b>checks the archive client-side</b> and retries the download if needed, until a <b>valid archive</b> has been transmitted.</li>

    <li>For the creation of a connection via HTTPS, more random values were required than were available right at the beginning. This meant that sometimes it took <b>several minutes</b> until the connection could be made. Using <a href="https://en.wikipedia.org/wiki/Entropy_(computing)#Linux_kernel">haveged</a> allows to more quickly build a stock of random values, which vastly <b>reduces the delay before the beginning</b> of an installation.</li>
       
    <li>The percentage for the <b>status bar</b> that indicates the progress of a client installation is now calculated and displayed correctly.</li>
       
    <li>The "Change client" dialog now also works when using it <b>multiple times in a row</b> with different clients.</li>
</ul>


<h3>Small changes</h3>

<ul>
    <li>The <i>"Real time client status"</i> is now able to also display <b>very large protocol files</b>.</li>
	
    <li>To work around issues that occurred when <b>fetching new tasks</b> from the m23 server, that were caused by systemd starting the network services with a delay, the m23 server is now pinged repeatedly until it replies, and only then the task list is requested.</li>
	
    <li>The <b>Linux kernel</b> for the boot media was updated to version 4.9.170.</li>
	
    <li><b>Scripts</b> that you <b>make available for other users</b> are now transferred using <b>HTTPS</b>.</li>
	
    <li>The <b>hardware database</b> is now created with an updated version of <a href="https://github.com/openSUSE/hwinfo/raw/master/src/ids/convert_hd">convert_hd</a>.</li>
	
    <li><b>Code that was used multiple times</b> for requesting information about the server (e.g. IP, network mask, Apache user etc.) was moved into bash scripts, that are now called from the places where they are needed.</li>
	
    <li>A <b>new automatic partitioning scheme</b>, which establishes partitions for system, swap and data in the first 500 Gb of a hard disk, has been added to the list of available choices.</li>
	
    <li>The <b>backup script</b> <i>m23Backup</i> now saves the IP address of the m23 server that it backs up.</li>
	
    <li>On UCS, the restoration script <i>m23Restore</i> will exchange all occurrances of the saved <b>m23 server IP address</b> by the current IP during <b>restoration</b> of the database.</li>
	
    <li>If the m23 client does not get assigned a valid task by the m23 server after booting from a boot medium, the client will now request information about the <b>network settings</b>.</li>
	
    <li>The <b>Kerberos configuration for the clients</b> has been adjusted, in order to also work with <b>UCS 4.3/4</b>.</li>

    <li><b>Many improvements on the source code</b> were made to reduce the number of warnings in the log files.</li>  

    <li><b>Error messages and warnings</b> that occur during the installation of m23 packages  (e.g. when starting server services or when recreating tables), are now logged to the file /m23/log/m23PostinstNonCriticalErrorMessages.log. These messages are not critical for m23, or are a sign of a successful previous configuration, but were sometimes causing confusion.</li>

    <li>The m23 web interface and the messages displayed on the clients' screens have undergone minor improvements, like <b>now showing the correct status</b> for packages that are marked for installation, or <b>colorized messages</b> during m23 client booting.</li>

    <li>The <b>delay on m23 server shutdown</b> caused by the <b>squid proxy</b> now lasts for maximally 5 seconds.</li>

    <li>Dialogs for <b>configuring Ubuntu 18.04 packages</b> that have debconf dialogs have been added to the m23 interface.</li>

    <li>m23 can no longer be used to install the <b>expired distributions</b> Linux Mint 17.1 Rebecca, Linux Mint 17.2 Rafaela, Linux Mint 17.3 Rosa, devuanjessie, trusty, HS-Fedora14, HS-opensuse11.4, HS-centos62, Ubuntu 12.04 Precise and Debian 7 Wheezy. When removing these distributions from m23, the corresponding debconf configuration dialogs have also been removed.</li>

    <li>The <b>plugin system</b> for extending the m23 interface from the early days of m23, which &mdash; as far as is known &mdash; is not currently being used anymore, has been <b>removed</b>. A replacement is available with <i>m23customPatch</i> since the release of m23 16.3.</li>
    
    <li>The automatical <b>recognition of suitable graphics settings</b> should be considerably more precise for all distributions.</li>

    <li>The <b>m23 remote administration service</b> has been <b>removed</b> from the menu of the m23 interface.</li>

    <li>A new <b>questionnaire</b> for sharing your <b>m23 references</b> has been created. A link to it is shown in the header of the German m23 web interface.</li>

    <li>When installing the m23 server from the m23 server installation ISO file, it is no longer attempted to install the <b>virtualization environment</b>. When needed, the corresponding package <i>m23-vbox</i> <b>can be installed manually</b>.</li>
</ul>

<h3>m23 clients and imaging</h3>

The options for the <b>creation</b> of images and for <b>installing</b> m23 clients from them have undergone massive changes. Features that couldn't reliably ensure a usable client have been removed during this rework.

<ul>
    <li>Instead of a <b>source MBR file</b> now <b><a href="https://en.wikipedia.org/wiki/GNU_GRUB">grub</a></b> will always be used.</li>
    <li>Only <b>partitions</b> (and no longer complete hard drives) are available as a <b>source</b> and <b>target for the image</b>, because installing complete hard drive images frequently caused issues.</li>
    <li>When recovering a partition image, the <b>file system</b> will automatically be <b>enlarged</b> (if the target partition is larger than the original partition). Ext file systems will now be resized when mounted.</li>
    <li><i>partcloneWrapper</i> is now able to recognize file systems again and will automatically choose the most suitable and <b>most space-saving image file format</b>.</li>
    <li>For compressing image files, a variant of <a href="https://en.wikipedia.org/wiki/Gzip">gzip</a> is available now that <b>uses a less efficient compression</b>, but <b>works faster</b>.</li>
    <li><b>m23 clients</b> that are being installed from image files will <b>only slightly be modified by m23</b> instead of going through a full Debian or Ubuntu configuration routine. The distribution base is automatically recognized (also works on Linux Mint) and the correct functions are called. The selection of the distribution in the m23 interface is no longer necessary (nor possible).</li>
    <li>The <b>package sources lists</b> (including /etc/apt/sources.list.d/*.list) of the m23 client are now <b>transferred to the m23 server</b> and will be compared to previous versions of the list. This makes it possible to <b>install packages</b> on imaging clients from the m23 interface.</li>
    <li>A new <b>warning</b> will be shown when the <b>"Imaging" pseudo-distribution</b> has been selected, as using images is the technically most challenging variant of installing an m23 client. It is not guaranteed that every image will result in a functional m23 client.</li>
</ul>

<h3>Downloads / Update</h3>

<p>The latest version is available as an update via the m23 interface, via APT (Note: the <b>package source server</b> for packages specific to m23 is now "<b>deb http://m23inst.goos-habermann.de ./</b>". Setup as described in the <a href="?id=1000000175#deb">installation guide</a>), as ISO file for creating an m23 server boot medium, as a preinstalled virtual machine or as an image file for RaspberryPi (the latter three are available from the <a href="index.php?currentpath=home/Downloads">download section</a>).</p>

<!-- bis 2019-05-22 -->
