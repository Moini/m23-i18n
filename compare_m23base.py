vars1 = []
vars2 = []

with open("m23-i18n/i18n/de/m23base.php") as file1:
    for line in file1:
        vn = line.split('=')[0].strip()
        vars1.append(vn)

with open("m23-i18n/i18n/fr/m23base.php") as file2:
    for line in file2:
        vn = line.split('=')[0].strip()
        vars2.append(vn)

for item in vars1:
    if item not in vars2:
        print(item)